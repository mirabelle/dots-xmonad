import XMonad
import XMonad.Util.EZConfig
import XMonad.Config.Desktop
import XMonad.Layout.Spacing 
import XMonad.Layout.NoBorders
import XMonad.Layout.Fullscreen
import XMonad.Hooks.ManageHelpers
import XMonad.Util.SpawnOnce

import qualified Data.Map as M

------------------------------------------------------------------------
-- Custom key bindings. 
--

sink = "`pacmd list-sinks | grep -B4 \"state: RUNNING\" | sed 1q | awk \'{print $NF}\'`"

myKeys conf = M.union (keys XMonad.def conf) $ mkKeymap conf $

  -- Audio
  [ ("<XF86AudioMute>", spawn $ "pactl set-sink-mute " ++ sink ++ " toggle")
  , ("<XF86AudioLowerVolume>", spawn $ "pactl set-sink-volume " ++ sink ++ " -5%")
  , ("<XF86AudioRaiseVolume>", spawn $ "pactl set-sink-volume " ++ sink ++ " +5%")

  -- Brightness
  , ("<XF86MonBrightnessUp>", spawn "lux -a 10%") 
  , ("<XF86MonBrightnessDown>", spawn "lux -s 10%") 

  , ("M-S-<Backspace>", spawn "xflock4")
  ] ++

  -- mod-[F1..F12], often used programs
  [(("M-<F" ++ show k ++ ">"), spawn $ i)
      | (i, k) <- zip 
	[ "rofi -show drun"
	, "qutebrowser"
	] [1..12]
  ]

------------------------------------------------------------------------
-- Custom layout
--

myLayout = desktopLayoutModifiers $ smartBorders $ tiled ||| Full
  where
     tiled   = smartSpacing freeSpace $ Tall master resize ratio

     freeSpace = 5
     master    = 1
     ratio     = toRational (2 / (1 + sqrt 5 :: Double))
     resize    = 3/100

myWorkspaces =  ["TTY","WEB","LAB","MED"]

myManageHook = composeAll 
  [ isFullscreen --> doFullFloat
  , fullscreenManageHook
  , manageHook desktopConfig ]

myHandleEventHook = composeAll 
  [ fullscreenEventHook
  , handleEventHook desktopConfig ]

myStartupHook = composeAll
  [ spawn "$HOME/.config/polybar/launch.sh"
  , spawn "feh --bg-scale --no-fehbg --randomize Pictures/Wallpapers/"
  , spawnOnce "xscreensaver -no-splash"
  , startupHook desktopConfig ]

------------------------------------------------------------------------
-- Combine it all together
--

main = xmonad desktopConfig {
    terminal           = "alacritty",
    focusFollowsMouse  = False,
    clickJustFocuses   = True,
    borderWidth        = 2,
    focusedBorderColor = "#6A5ACD",

    modMask            = mod4Mask,
    workspaces         = myWorkspaces,
    keys               = myKeys,

    layoutHook         = myLayout,
    manageHook         = myManageHook,
    handleEventHook    = myHandleEventHook,
    startupHook        = myStartupHook
}

